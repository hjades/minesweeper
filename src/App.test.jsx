import { render } from '@testing-library/react';
import { expect } from 'chai';
import * as React from 'react';
import App from './App';

describe('<App>', () => {
  it('renders Minesweeper text', () => {
    const { getByText } = render(<App />);
    const text = getByText(/minesweeper/i);
    expect(document.body.contains(text));
  });
});
