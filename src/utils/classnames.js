const getClassName = (className, inUse) => (inUse ? className : '');

const objectToArray = (obj) => {
  const classNamesArr = Object.entries(obj)
    .map(([className, inUse]) => getClassName(className, inUse))
    .filter(Boolean);

  return classNamesArr;
};

const getClassNames = (...classNames) => {
  const result = [];
  for (const className of classNames) {
    if (typeof className === 'object') {
      result.push(...objectToArray(className));
    } else if (className) {
      result.push(className);
    }
  }

  return result.join(' ');
};

export default getClassNames;
