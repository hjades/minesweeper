import Text from 'components/text';
import React from 'react';
import getClassNames from 'utils/classnames';
import { TILE } from '../constants';
import Closed from './closed';
import styles from './styles.module.css';

const Flag = ({ className = '' }) => (
  <Closed>
    <div className={getClassNames(styles.tile, styles.flag, className)}>
      <Text>{TILE.flag}</Text>
    </div>
  </Closed>
);

export default Flag;
