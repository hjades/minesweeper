import Text from 'components/text';
import React from 'react';
import getClassNames from 'utils/classnames';
import styles from './styles.module.css';

const Number = ({ children, className = '' }) => (
  <div className={getClassNames(styles.tile, className)}>
    <Text>{children}</Text>
  </div>
);

export default Number;
