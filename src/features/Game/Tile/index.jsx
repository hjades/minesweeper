import React, { memo } from 'react';
import { TILE } from '../constants';
import Square from '../Square';
import Closed from './closed';
import Flag from './flag';
import Mine from './mine';
import Number from './number';

const Tile = memo(
  ({ value = '', onClick = () => {} }) => {
    const TileComponent = () => {
      switch (value) {
        case TILE.closed:
          return <Closed />;
        case TILE.blank:
          return null;
        case TILE.flag:
          return <Flag />;
        case TILE.mine:
          return <Mine />;
        default:
          return <Number>{value}</Number>;
      }
    };

    return (
      <Square onClick={onClick}>
        <TileComponent />
      </Square>
    );
  },
  (prev, next) => prev.value === next.value,
);

export default Tile;
