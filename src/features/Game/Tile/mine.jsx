import Text from 'components/text';
import React from 'react';
import getClassNames from 'utils/classnames';
import { TILE } from '../constants';
import styles from './styles.module.css';

// TODO: decide between 💣 and 🧨

const Mine = ({ className = '' }) => (
  <div className={getClassNames(styles.tile, className)}>
    <Text>{TILE.mine}</Text>
  </div>
);

export default Mine;
