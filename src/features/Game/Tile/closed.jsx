import React from 'react';
import getClassNames from 'utils/classnames';
import styles from './styles.module.css';

const Closed = ({ children }) => (
  <>
    <div className={getClassNames(styles.closed)} />
    {children}
  </>
);

export default Closed;
