import { useReducer } from 'react';
import { STATUS, TILE } from '../constants';
import { generateMines, isEndGame, updateTiles } from './utils';

const initState = (level) => ({
  level,
  flags: 0,
  mines: [],
  tiles: Object.fromEntries(Object.entries(new Array(level.grid).fill(''))),
  status: STATUS.initial,
});

const reducer = (state, action) => {
  if (action.type === 'restart') {
    return initState(state.level);
  }
  if (state.status === STATUS.end) {
    return state;
  }
  // TODO: implement change level
  switch (action.type) {
    case 'click':
    case 'keypress': {
      const id = Number(action.payload);
      let { mines } = state;
      // generate mines on the first click
      if (!mines.length) {
        mines = generateMines(state.level.mines, state.level.grid, id);
      }
      if (state.tiles[id] === TILE.closed) {
        const tiles = updateTiles(state.tiles, mines, id);
        const status = isEndGame(tiles, mines, state.flags, state.level.mines)
          ? STATUS.end
          : STATUS.playing;
        return { ...state, mines, tiles, status };
      }
      return state;
    }
    case 'contextmenu': {
      const id = Number(action.payload);
      const currentTile = state.tiles[id];
      if (currentTile === TILE.flag) {
        return {
          ...state,
          flags: state.flags - 1,
          tiles: { ...state.tiles, [id]: TILE.closed },
        };
      }
      if (currentTile === TILE.closed && state.flags < state.level.mines) {
        const flags = state.flags + 1;
        const tiles = { ...state.tiles, [id]: TILE.flag };
        const status = isEndGame(tiles, state.mines, flags, state.level.mines)
          ? STATUS.end
          : STATUS.playing;
        return {
          ...state,
          status,
          flags,
          tiles,
        };
      }
      return state;
    }
    default:
      throw new Error();
  }
};

const useMinesweeper = (level) => {
  const [state, dispatch] = useReducer(reducer, level, initState);

  return { state, dispatch };
};

export default useMinesweeper;
