import { expect } from 'chai';
import { DIRECTION, TILE } from '../constants';
import {
  calcAdjMines,
  generateMines,
  getGridLength,
  getMoves,
  isEndGame,
  isMine,
  isValidMove,
} from './utils';

describe('Minesweeper Game Utils', () => {
  describe('isMine', () => {
    it('should return Boolean for all kind of inputs', () => {
      expect(isMine(0)).to.be.a('boolean');
      expect(isMine('')).to.be.a('boolean');
      expect(isMine(undefined)).to.be.a('boolean');
    });
    it('should return true when input is 1', () => {
      expect(isMine(1)).to.equal(true);
    });
    it('should return false when input is 0', () => {
      expect(isMine(0)).to.equal(false);
    });
  });
  describe('isValidMove', () => {
    const gridLength = 3;
    it('should return false if out of range', () => {
      expect(isValidMove(-1, DIRECTION.S), gridLength).to.equal(false);
      expect(
        isValidMove(gridLength * gridLength, DIRECTION.SN, gridLength),
      ).to.equal(false);
    });
    describe('when dir towards East', () => {
      it('should return false when tile at right side (position + 1 can be fully divided by gridLength)', () => {
        expect(isValidMove(gridLength - 1, DIRECTION.E, gridLength)).to.equal(
          false,
        );
      });
      it('should return true when tile not at right side (position + 1 can not be fully divided by gridLength)', () => {
        expect(isValidMove(gridLength, DIRECTION.E, gridLength)).to.equal(true);
      });
    });
    describe('when dir towards North', () => {
      it('should return false when tile at top side (position is smaller than gridLength)', () => {
        expect(isValidMove(gridLength - 1, DIRECTION.N, gridLength)).to.equal(
          false,
        );
      });
      it('should return true when tile not at top (position is bigger than gridLength)', () => {
        expect(isValidMove(gridLength + 1, DIRECTION.N, gridLength)).to.equal(
          true,
        );
      });
    });
    describe('when dir towards South', () => {
      it('should return false when tile at bottom (position is bigger than gridLength * (gridLength - 1))', () => {
        expect(
          isValidMove(gridLength * gridLength - 1, DIRECTION.S, gridLength),
        ).to.equal(false);
      });
      it('should return true when tile not at bottom (position is bigger than gridLength)', () => {
        expect(isValidMove(gridLength, DIRECTION.S, gridLength)).to.equal(true);
      });
    });
    describe('when dir towards West', () => {
      it('should return false when tile at left side (position + 1 divided by gridLength remains 1)', () => {
        expect(isValidMove(0, DIRECTION.W, gridLength)).to.equal(false);
      });
      it('should return true when tile not at left (position + 1 divided by gridLength does not remain 1)', () => {
        expect(isValidMove(gridLength - 1, DIRECTION.W, gridLength)).to.equal(
          true,
        );
      });
    });
  });
  describe('getGridLength', () => {
    it('should return the root square of inputs', () => {
      expect(getGridLength(0)).to.equal(0);
      expect(getGridLength(3 * 3)).to.equal(3);
      expect(getGridLength(5 * 5)).to.equal(5);
    });
  });
  describe('getMoves', () => {
    const moves = getMoves(3);
    it('should return an array with length of 8', () => {
      expect(moves).to.be.an('array').that.have.lengthOf(8);
    });
    it('should return the possible moves', () => {
      expect(moves).to.deep.include({ dir: DIRECTION.E, count: +1 });
      expect(moves).to.deep.include({ dir: DIRECTION.S, count: +3 });
      expect(moves).to.deep.include({ dir: DIRECTION.W, count: -1 });
      expect(moves).to.deep.include({ dir: DIRECTION.N, count: -3 });
      expect(moves).to.deep.include({ dir: DIRECTION.NE, count: -2 });
      expect(moves).to.deep.include({ dir: DIRECTION.SE, count: +4 });
      expect(moves).to.deep.include({ dir: DIRECTION.SW, count: +2 });
      expect(moves).to.deep.include({ dir: DIRECTION.NW, count: -4 });
    });
  });
  describe('generateMines', () => {
    const mineCount = 4;
    const gridCount = 5;
    const startPos = 2;
    const mines = generateMines(mineCount, gridCount, startPos);
    it('should return an array with length of gridCount', () => {
      expect(mines).to.be.an('array').that.have.lengthOf(5);
    });
    it('should be blank (0) at startPos', () => {
      expect(mines[startPos]).to.equal(0);
    });
  });
  describe('calcAdjMines', () => {
    const mines = [
      ...[1, 0, 0, 0],
      ...[0, 0, 0, 0],
      ...[0, 0, 0, 0],
      ...[1, 0, 1, 1],
    ];
    it('should return the correct number', () => {
      expect(calcAdjMines(mines, 0)).to.equal(0);
      expect(calcAdjMines(mines, 1)).to.equal(1);
      expect(calcAdjMines(mines, 2)).to.equal(0);
      expect(calcAdjMines(mines, 3)).to.equal(0);
      expect(calcAdjMines(mines, 4)).to.equal(1);
      expect(calcAdjMines(mines, 5)).to.equal(1);
      expect(calcAdjMines(mines, 6)).to.equal(0);
      expect(calcAdjMines(mines, 7)).to.equal(0);
      expect(calcAdjMines(mines, 8)).to.equal(1);
      expect(calcAdjMines(mines, 9)).to.equal(2);
      expect(calcAdjMines(mines, 10)).to.equal(2);
      expect(calcAdjMines(mines, 11)).to.equal(2);
      expect(calcAdjMines(mines, 12)).to.equal(0);
      expect(calcAdjMines(mines, 13)).to.equal(2);
      expect(calcAdjMines(mines, 14)).to.equal(1);
      expect(calcAdjMines(mines, 15)).to.equal(1);
    });
  });
  describe('isEndGame', () => {
    it('should return false when minesArr is empty', () => {
      expect(isEndGame({}, [], 0, 0)).to.equal(false);
    });
    it('should return false if flagCount is not equal to mineCount', () => {
      expect(
        isEndGame({ 0: TILE.closed, 1: TILE.closed }, [1, 0], 0, 2),
      ).to.equal(false);
    });
    it('should return false if flagged a wrong tile', () => {
      expect(
        isEndGame({ 0: TILE.closed, 1: TILE.flag }, [1, 0], 1, 1),
      ).to.equal(false);
    });
    it('should return true when a mine is revealed', () => {
      expect(
        isEndGame({ 0: TILE.closed, 1: TILE.mine }, [1, 1], 0, 2),
      ).to.equal(true);
    });
    it('should return true when every mine is flagged', () => {
      expect(isEndGame({ 0: TILE.flag, 1: TILE.flag }, [1, 1], 2, 2)).to.equal(
        true,
      );
    });
  });
});
