import { DIRECTION, TILE } from '../constants';

export const isMine = (mine) => !!mine;

export const isValidMove = (position, dir, gridLength) => {
  // check range
  if (position < 0 || position >= gridLength * gridLength) {
    return false;
  }
  const checklist = [];
  if (dir.includes(DIRECTION.E)) {
    checklist.push((position + 1) % gridLength !== 0); // right can't go east
  }
  if (dir.includes(DIRECTION.S)) {
    checklist.push(position < gridLength * (gridLength - 1)); // bottom can't go south
  }
  if (dir.includes(DIRECTION.W)) {
    checklist.push((position + 1) % gridLength !== 1); // left can't go west
  }
  if (dir.includes(DIRECTION.N)) {
    checklist.push(position >= gridLength); // top can't go north
  }
  // a valid move needs to pass each condition in checklist
  return checklist.every(Boolean);
};

export const getGridLength = (gridCount) => Math.sqrt(gridCount);

// get all the adjacent moves count from the current tile
export const getMoves = (gridLength) => {
  const moves = [
    { dir: DIRECTION.E, count: +1 },
    { dir: DIRECTION.S, count: +gridLength },
    { dir: DIRECTION.W, count: -1 },
    { dir: DIRECTION.N, count: -gridLength },
    { dir: DIRECTION.NE, count: -(gridLength - 1) },
    { dir: DIRECTION.SE, count: +(gridLength + 1) },
    { dir: DIRECTION.SW, count: +(gridLength - 1) },
    { dir: DIRECTION.NW, count: -(gridLength + 1) },
  ];
  return moves;
};

// Fisher-Yates shuffle
const shuffle = (inputArray) => {
  const array = [...inputArray];
  for (let i = array.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

export const generateMines = (mineCount, gridCount, startPosition = 0) => {
  const array = shuffle([
    ...new Array(mineCount).fill(1),
    ...new Array(gridCount - mineCount).fill(0),
  ]);
  // starting tile cannot be mine, so swap them
  if (isMine(array[startPosition])) {
    const firstBlankIdx = array.findIndex((a) => !isMine(a));
    array[startPosition] = 0;
    array[firstBlankIdx] = 1;
  }
  return array;
};

export const calcAdjMines = (minesArr, position) => {
  const gridLength = getGridLength(minesArr.length);
  const moves = getMoves(gridLength);
  const count = moves.reduce(
    (acc, move) =>
      acc +
      (isValidMove(position, move.dir, gridLength) &&
        isMine(minesArr[position + move.count])),
    0,
  );
  return count;
};

export const updateTiles = (tilesObj, minesArr, position) => {
  let tiles = { ...tilesObj };
  // if mine, display mine
  if (isMine(minesArr[position])) {
    tiles[position] = TILE.mine;
    return tiles;
  }
  const adjMines = calcAdjMines(minesArr, position);
  tiles[position] = adjMines;
  // if has adjacent mines, display number of adjacent mines
  if (adjMines) {
    return tiles;
  }
  const gridLength = getGridLength(minesArr.length);
  const moves = getMoves(gridLength);
  moves.forEach((move) => {
    const newPos = position + move.count;
    // if new tile is closed then recurse
    if (
      isValidMove(position, move.dir, gridLength) &&
      tiles[newPos] === TILE.closed
    ) {
      tiles = updateTiles(tiles, minesArr, newPos);
    }
  });
  return tiles;
};

export const isEndGame = (tilesObj, minesArr, flagCount, mineCount) => {
  // if mines haven't been generated, then keep playing
  if (!minesArr.length) {
    return false;
  }
  // if user revealed a mine, game ends
  if (Object.values(tilesObj).some((tile) => tile === TILE.mine)) {
    return true;
  }
  // if user flagged all the mines
  if (
    !(
      flagCount !== mineCount ||
      minesArr.some((mine, idx) =>
        mine ? tilesObj[idx] !== TILE.flag : tilesObj[idx] === TILE.flag,
      )
    )
  ) {
    return true;
  }
  return false;
};
