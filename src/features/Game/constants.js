export const LEVEL = {
  easy: { name: 'easy', grid: 9 * 9, mines: 10 },
  medium: { name: 'medium', grid: 16 * 16, mines: 40 },
  hard: { name: 'hard', grid: 24 * 24, mines: 99 },
};

export const TILE = {
  flag: '🚩',
  mine: '💣',
  blank: 0,
  closed: '',
};

export const DIRECTION = {
  E: 'E',
  S: 'S',
  W: 'W',
  N: 'N',
  NE: 'NE',
  SE: 'SE',
  SW: 'SW',
  NW: 'NW',
};

export const STATUS = {
  initial: 0,
  playing: 1,
  end: 2,
};
