import React from 'react';
import { STATUS } from '../constants';
import FlagCounter from './counter';
import StatusText from './text';

const Status = ({ status = STATUS.initial, flags = 0, mines = 0 }) => {
  if (status === STATUS.end) {
    return <StatusText win={flags === mines} />;
  }
  return <FlagCounter flags={flags} mines={mines} />;
};

export default Status;
