import Text from 'components/text';
import React from 'react';
import getClassNames from 'utils/classnames';
import styles from './styles.module.css';

const StatusText = ({ win = false }) => (
  <Text
    gutterBottom
    className={getClassNames(styles.status, { [styles.win]: win })}
  >
    YOU {win ? 'WIN' : 'LOSE'}
  </Text>
);

export default StatusText;
