import Text from 'components/text';
import React from 'react';

const FlagCounter = ({ flags = 0, mines = 0 }) => (
  <Text gutterBottom>
    Flags: {flags} / {mines}
  </Text>
);

export default FlagCounter;
