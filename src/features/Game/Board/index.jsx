import React from 'react';
import getClassNames from 'utils/classnames';
import styles from './styles.module.css';

const Board = ({ children, className = '', level = 'easy' }) => (
  <div className={getClassNames(styles.board, styles[level], className)}>
    {children}
  </div>
);

export default Board;
