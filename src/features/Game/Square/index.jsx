import React from 'react';
import getClassNames from 'utils/classnames';
import styles from './styles.module.css';

const Square = ({ children, className = '', onClick = () => {} }) => (
  <div
    role="button"
    tabIndex={0}
    onClick={onClick}
    onKeyPress={onClick}
    onContextMenu={onClick}
    className={getClassNames(styles.square, className)}
  >
    {children}
  </div>
);

export default Square;
