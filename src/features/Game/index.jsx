import Button from 'components/button';
import React, { useCallback } from 'react';
import Board from './Board';
import { LEVEL } from './constants';
import useMinesweeper from './hooks/useMinesweeper';
import Status from './Status';
import Tile from './Tile';

const Game = () => {
  const { state, dispatch } = useMinesweeper(LEVEL.easy);

  const handleClick = useCallback(
    (payload) => (e) => {
      dispatch({ type: e.type, payload });
      e.preventDefault();
      e.currentTarget.blur();
    },
    [],
  );

  return (
    <div>
      <Status
        flags={state.flags}
        status={state.status}
        mines={state.level.mines}
      />
      <Board level={state.level.name}>
        {Object.entries(state.tiles).map(([id, value]) => (
          <Tile
            key={`${value}-${id}`}
            value={value}
            onClick={handleClick(id)}
          />
        ))}
      </Board>
      <Button onClick={() => dispatch({ type: 'restart' })}>RESTART</Button>
    </div>
  );
};

export default Game;
