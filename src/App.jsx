import Text from 'components/text';
import Game from 'features/Game';
import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <Text variant="h6" gutterBottom>
        Minesweeper
      </Text>
      <Game />
      <Text>Reveal a tile: Left click</Text>
      <Text>Flag a tile: Right click (Long press on touch devices)</Text>
    </div>
  );
}

export default App;
