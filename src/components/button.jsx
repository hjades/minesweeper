import Text from 'components/text';
import React from 'react';
import getClassNames from 'utils/classnames';
import styles from './button.module.css';

const Button = ({
  children,
  className = '',
  variant = 'filled',
  onClick = () => {},
}) => (
  <button
    type="button"
    onClick={onClick}
    className={getClassNames(styles[variant], className)}
  >
    <Text>{children}</Text>
  </button>
);

export default Button;
