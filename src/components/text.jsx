import React from 'react';
import getClassNames from 'utils/classnames';
import styles from './text.module.css';

const Text = ({
  children,
  className = '',
  variant = 'body1',
  gutterBottom = false,
}) => {
  const Tag = variant === 'h6' ? 'h6' : 'p';

  return (
    <Tag
      className={getClassNames(
        styles[variant],
        { [styles.gutterBottom]: gutterBottom },
        className,
      )}
    >
      {children}
    </Tag>
  );
};

export default Text;
